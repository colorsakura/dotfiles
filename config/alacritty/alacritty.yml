# Reference: https://github.com/alacritty/alacritty/blob/master/alacritty.yml

window:
  # Window dimensions (changes require restart)
  dimensions:
    columns: 120
    lines: 30
  # Window padding (changes require restart)
  padding:
    x: 2
    y: 2
  # Window decorations
  decorations: None  # full/none/buttonless/transparent
  startup_mode: Windowed # Maximized/Windowed/Fullscreen

scrolling:
  # Maximum number of lines in the scrollback buffer, 0 to disable scrolling.
  history: 15000
  # Number of lines the viewport will move for every line scrolled when
  multiplier: 3

font:
    normal:
      family: Jetbrains Mono
      style: Regular
    size: 12
    #  bold:
    #    family: 
    #  italic:
    #    family: 

    # Offset is the extra space around each character. `offset.y` can be thought of
    # as modifying the line spacing, and `offset.x` as modifying the letter spacing.
    offset:
      x: 0
      y: 0

    # Glyph offset determines the locations of the glyphs within their cells with
    # the default being at the bottom. Increasing `x` moves the glyph to the right,
    # increasing `y` moves the glyph upwards.
    glyph_offset:
      x: 0
      y: 0



debug:
  # Display the time it takes to redraw each frame.
  render_timer: false

# Use custom cursor colors. If `true`, the `colors.cursor.foreground` and
# `colors.cursor.background` colors will be used to display the cursor.
# Otherwise the cell colors are inverted for the cursor.
# custom_cursor_colors: true

# If `true`, bold text is drawn using the bright color variants.
draw_bold_text_with_bright_colors: true

# TokyoNight Alacritty Colors
colors:
  # Default colors
  primary:
    background: '0x1a1b26'
    foreground: '0xc0caf5'

  # Normal colors
  normal:
    black:   '0x15161e'
    red:     '0xf7768e'
    green:   '0x9ece6a'
    yellow:  '0xe0af68'
    blue:    '0x7aa2f7'
    magenta: '0xbb9af7'
    cyan:    '0x7dcfff'
    white:   '0xa9b1d6'

  # Bright colors
  bright:
    black:   '0x414868'
    red:     '0xf7768e'
    green:   '0x9ece6a'
    yellow:  '0xe0af68'
    blue:    '0x7aa2f7'
    magenta: '0xbb9af7'
    cyan:    '0x7dcfff'
    white:   '0xc0caf5'

  indexed_colors:
    - { index: 16, color: '0xff9e64' }
    - { index: 17, color: '0xdb4b4b' }

# Visual Bell
bell:
  animation: EaseOutExpo
  duration: 0  # disable visual bell

# Background opacity
windws_opacity: 1.0

mouse_bindings:
  - { mouse: Middle, action: PasteSelection }

mouse:
  double_click: { threshold: 300 }
  triple_click: { threshold: 300 }
  cursor_when_typing: true

selection:
  semantic_escape_chars: ",│`|:\"' ()[]{}<>"

  # When set to `true`, selected text will be copied to the primary clipboard.
  save_to_clipboard: false

window.dynamic_title: false

# Cursor style
cursor:
  style: Beam # Block/Underline/Beam
  # If this is `true`, the cursor will be rendered as a hollow box when the
  # window is not focused.
  unfocused_hollow: true
  vi_mode_style:
    shape: Beam

live_config_reload: true

shell:
  program: /bin/fish

key_bindings:
  - { key: V,        mods: Command, action: Paste                        }
  - { key: C,        mods: Command, action: Copy                         }
  - { key: H,        mods: Command, action: Hide                         }
  - { key: Q,        mods: Command, action: Quit                         }
  - { key: W,        mods: Command, action: Quit                         }
  - { key: Back,                    chars: "\x7f"                        }
  - { key: Insert,                  chars: "\x1b[2~"                     }
  - { key: Delete,                  chars: "\x1b[3~"                     }
  - { key: Left,                    chars: "\x1b[D",   mode: ~AppCursor  }
  - { key: Left,                    chars: "\x1bOD",   mode: AppCursor   }
  - { key: Right,                   chars: "\x1b[C",   mode: ~AppCursor  }
  - { key: Right,                   chars: "\x1bOC",   mode: AppCursor   }
  # https://github.com/alacritty/alacritty/issues/93
  - { key: F,  mods: Alt,           chars: "\x1bf"                       }
  - { key: B,  mods: Alt,           chars: "\x1bb"                       }
