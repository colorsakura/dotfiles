require('opt.basic')
require('keymaps')
require('plugins')


-- plugins
require('opt.treesitter')
require('opt.nvim-tree')
require('opt.lualine')
require('opt.todo-comments')
-- require('opt.luasnip')
-- require('opt.telescope')
-- require('opt.mason')
-- require('opt.null-ls')
-- require('opt.comment')

-- require('lsp.setup')
-- require('lsp.lua')
-- require('lsp.c')
-- require('lsp.python')

vim.cmd[[colorscheme tokyonight]]
