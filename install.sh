#!/bin/env bash

config_dirs=(fish mpv fontconfig ranger alacritty waybar mako wayfire.ini)
config_files=()
core_packages=(git curl fish neovim alacritty)
gnome_desktop_packages=()


# 应用配置文件
function install_config() {
  # .config 
  for dir in ${config_dirs[*]}; do
      rm -rf $HOME/.config/$dir
      ln -s $PWD/config/$dir $HOME/.config/
      echo "${dir} 链接成功..."
  done

  # ~
  for file in ${config_files[*]}; do
      rm ~/.$file
      ln -s $PWD/$file $HOME/.$file
      echo "${file} 链接成功..."
  done
}


function main() { 
  install_config
}

function install_package() {
  #TODO: auto install my arch packages 
  echo "OK"
}

function check_permission() {
  #TODO: check root permission
}

# run
main
