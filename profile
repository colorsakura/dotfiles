export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus
# 解决 Jetbrains 中文输入问题
export LC_CTYPE=zh_CN.UTF-8
ibus-daemon -d -x